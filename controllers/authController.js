const passport = require('passport');
const bcryptjs = require('bcryptjs');
const nodemailer = require('nodemailer');
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const jwt = require('jsonwebtoken');
const JWT_KEY = "jwtactive1488";
const JWT_RESET_KEY = "jwtreset1488";


const User = require('../models/User');


exports.registerHandle = (req, res) => {
    const { name, email, password, password2 } = req.body;
    let errors = [];


    if (!name || !email || !password || !password2) {
        errors.push({ msg: 'Заполните все поля' });
    }


    if (password != password2) {
        errors.push({ msg: 'Пароли не совпадают' });
    }


    if (password.length < 8) {
        errors.push({ msg: 'Пароль должен быть более 8 символов.' });
    }

    if (errors.length > 0) {
        res.render('register', {
            errors,
            name,
            email,
            password,
            password2
        });
    } else {

        User.findOne({ email: email }).then(user => {
            if (user) {

                errors.push({ msg: 'Почта занята.' });
                res.render('register', {
                    errors,
                    name,
                    email,
                    password,
                    password2
                });
            } else {

                const oauth2Client = new OAuth2(
                    "1023811724420-ni832rec8r0n1otj6k0v67im8v27mcnq.apps.googleusercontent.com", // ClientID
                    "JyXDnnKuQ9gJJso1DQJaQouD", // Client Secret
                    "https://developers.google.com/oauthplayground" // Redirect URL
                );

                oauth2Client.setCredentials({
                    refresh_token: "1//041bXHcqdPbxZCgYIARAAGAQSNwF-L9Ir8ha281R-rxMFt8QMdxWWBksCTlUeJ7UD6V0EjAEoAwjBitqftDSeK1qQEhtqp_0Q-Mg"
                });
                const accessToken = oauth2Client.getAccessToken()
                console.log('\n\n\n' + accessToken);
                const token = jwt.sign({ name, email, password }, JWT_KEY, { expiresIn: '30m' });
                const CLIENT_URL = 'http://' + req.headers.host;

                const output = `
                <h2>Нажмите на ссылку ниже, чтобы подтвердить ваш аккаунт</h2>
                <p>${CLIENT_URL}/auth/activate/${token}</p>
                <p><b>NOTE: </b> Срок действия ссылки 30 минут.</p>
                `;

                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        type: "OAuth2",
                        user: "juniorhdblr@gmail.com",
                        clientId: "1023811724420-ni832rec8r0n1otj6k0v67im8v27mcnq.apps.googleusercontent.com",
                        clientSecret: "JyXDnnKuQ9gJJso1DQJaQouD",
                        refreshToken: "1//041bXHcqdPbxZCgYIARAAGAQSNwF-L9Ir8ha281R-rxMFt8QMdxWWBksCTlUeJ7UD6V0EjAEoAwjBitqftDSeK1qQEhtqp_0Q-Mg",
                        accessToken: accessToken,
                    },
                });

                
                const mailOptions = {
                    from: '"Auth Admin" <juniorhdblr@gmail.com>', 
                    to: email, 
                    subject: "Account Verification: NodeJS Auth ✔", 
                    generateTextFromHTML: true,
                    html: output, 
                };

                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        console.log(error);
                        req.flash(
                            'error_msg',
                            'Что-то не так :('
                        );
                        res.redirect('/auth/login');
                    }
                    else {
                        console.log('Mail sent : %s', info.response);
                        req.flash(
                            'success_msg',
                            'Ссылка для активации отправлена на почту.'
                        );
                        res.redirect('/auth/login');
                    }
                })

            }
        });
    }
}


exports.activateHandle = (req, res) => {
    const token = req.params.token;
    let errors = [];
    if (token) {
        jwt.verify(token, JWT_KEY, (err, decodedToken) => {
            if (err) {
                req.flash(
                    'error_msg',
                    'Неверная ссылка или действие ссылки истекло! Попробуйте снова.'
                );
                res.redirect('/auth/register');
            }
            else {
                const { name, email, password } = decodedToken;
                User.findOne({ email: email }).then(user => {
                    if (user) {
                        req.flash(
                            'error_msg',
                            'Такая почта уже используется! Пожалуйста залогиньтесь.'
                        );
                        res.redirect('/auth/login');
                    } else {
                        const newUser = new User({
                            name,
                            email,
                            password
                        });

                        bcryptjs.genSalt(62, (err, salt) => {
                            bcryptjs.hash(newUser.password, salt, (err, hash) => {
                                if (err) throw err;
                                newUser.password = hash;
                                newUser
                                    .save()
                                    .then(user => {
                                        req.flash(
                                            'success_msg',
                                            'Аккаунт верифицирован. Вы можете войти'
                                        );
                                        res.redirect('/auth/login');
                                    })
                                    .catch(err => console.log(err));
                            });
                        });
                    }
                });
            }

        })
    }
    else {
        console.log("Ошибка активации аккаунта!")
    }
}


exports.forgotPassword = (req, res) => {
    const { email } = req.body;

    let errors = [];


    if (!email) {
        errors.push({ msg: 'Введите почту!' });
    }

    if (errors.length > 0) {
        res.render('forgot', {
            errors,
            email
        });
    } else {
        User.findOne({ email: email }).then(user => {
            if (!user) {

                errors.push({ msg: 'Такая почта не найдена!' });
                res.render('forgot', {
                    errors,
                    email
                });
            } else {

                const oauth2Client = new OAuth2(
                    "1023811724420-ni832rec8r0n1otj6k0v67im8v27mcnq.apps.googleusercontent.com", // ClientID
                    "JyXDnnKuQ9gJJso1DQJaQouD", // Client Secret
                    "https://developers.google.com/oauthplayground" // Redirect URL
                );

                oauth2Client.setCredentials({
                    refresh_token: "1//041bXHcqdPbxZCgYIARAAGAQSNwF-L9Ir8ha281R-rxMFt8QMdxWWBksCTlUeJ7UD6V0EjAEoAwjBitqftDSeK1qQEhtqp_0Q-Mg"
                });
                const accessToken = oauth2Client.getAccessToken()

                const token = jwt.sign({ _id: user._id }, JWT_RESET_KEY, { expiresIn: '30m' });
                const CLIENT_URL = 'http://' + req.headers.host;
                const output = `
                <h2>Нажмите на ссылку ниже, чтобы сбросить ваш пароль</h2>
                <p>${CLIENT_URL}/auth/forgot/${token}</p>
                <p><b>NOTE: </b> Срок действия ссылки 30 минут.</p>
                `;

                User.updateOne({ resetLink: token }, (err, success) => {
                    if (err) {
                        errors.push({ msg: 'Ошибка смены пароля!' });
                        res.render('forgot', {
                            errors,
                            email
                        });
                    }
                    else {
                        const transporter = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                                type: "OAuth2",
                                user: "juniorhdblr@gmail.com",
                                clientId: "1023811724420-ni832rec8r0n1otj6k0v67im8v27mcnq.apps.googleusercontent.com",
                                clientSecret: "JyXDnnKuQ9gJJso1DQJaQouD",
                                refreshToken: "1//041bXHcqdPbxZCgYIARAAGAQSNwF-L9Ir8ha281R-rxMFt8QMdxWWBksCTlUeJ7UD6V0EjAEoAwjBitqftDSeK1qQEhtqp_0Q-Mg",
                                accessToken: accessToken
                            },
                        });

                        const mailOptions = {
                            from: '"Auth Admin" <juniorhdblr@gmail.com>',
                            to: email, 
                            subject: "Account Password Reset: NodeJS Auth ✔", 
                            html: output, 
                        };

                        transporter.sendMail(mailOptions, (error, info) => {
                            if (error) {
                                console.log(error);
                                req.flash(
                                    'error_msg',
                                    'Что-то не так :('
                                );
                                res.redirect('/auth/forgot');
                            }
                            else {
                                console.log('Mail sent : %s', info.response);
                                req.flash(
                                    'success_msg',
                                    'Ссылка для сброса пароля отправлена на почту.'
                                );
                                res.redirect('/auth/login');
                            }
                        })
                    }
                })

            }
        });
    }
}

exports.gotoReset = (req, res) => {
    const { token } = req.params;

    if (token) {
        jwt.verify(token, JWT_RESET_KEY, (err, decodedToken) => {
            if (err) {
                req.flash(
                    'error_msg',
                    'Срок действия ссылки истек.'
                );
                res.redirect('/auth/login');
            }
            else {
                const { _id } = decodedToken;
                User.findById(_id, (err, user) => {
                    if (err) {
                        req.flash(
                            'error_msg',
                            'Пользователя с такой почтой нет :('
                        );
                        res.redirect('/auth/login');
                    }
                    else {
                        res.redirect(`/auth/reset/${_id}`)
                    }
                })
            }
        })
    }
    else {
        console.log("Ошибка сброса пароля!")
    }
}


exports.resetPassword = (req, res) => {
    var { password, password2 } = req.body;
    const id = req.params.id;
    let errors = [];

    if (!password || !password2) {
        req.flash(
            'error_msg',
            'Заполните все поля.'
        );
        res.redirect(`/auth/reset/${id}`);
    }

    else if (password.length < 8) {
        req.flash(
            'error_msg',
            'Пароль должен быть более 8 символов.'
        );
        res.redirect(`/auth/reset/${id}`);
    }

    else if (password != password2) {
        req.flash(
            'error_msg',
            'Пароли не совпадают.'
        );
        res.redirect(`/auth/reset/${id}`);
    }

    else {
        bcryptjs.genSalt(10, (err, salt) => {
            bcryptjs.hash(password, salt, (err, hash) => {
                if (err) throw err;
                password = hash;

                User.findByIdAndUpdate(
                    { _id: id },
                    { password },
                    function (err, result) {
                        if (err) {
                            req.flash(
                                'error_msg',
                                'Ошибка сброса пароля'
                            );
                            res.redirect(`/auth/reset/${id}`);
                        } else {
                            req.flash(
                                'success_msg',
                                'Пароль успешно сброшен!'
                            );
                            res.redirect('/auth/login');
                        }
                    }
                );

            });
        });
    }
}


exports.loginHandle = (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/auth/login',
        failureFlash: true
    })(req, res, next);
}


exports.logoutHandle = (req, res) => {
    req.logout();
    req.flash('success_msg', 'Вы вышли из аккаунта');
    res.redirect('/auth/login');
}